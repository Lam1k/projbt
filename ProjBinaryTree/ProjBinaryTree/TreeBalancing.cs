﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjBinaryTree
{
    class TreeBalancing : BinaryTree
    {
        public static void RotateRR(ref BinaryTree root)
        {
            BinaryTree buf = root.Right.Left;


            if (root.Parent == null)
            {
                root.Right.Parent = null;
            }
            else
            {
                root.Right.Parent = root.Parent;
                if (root.Parent.Left == root)
                {
                    root.Parent.Left = root.Right;
                }
                else
                {
                    root.Parent.Right = root.Right;
                }
            }
            root.Parent = root.Right;
            root.Right.Left = root;
            root.Right = buf;
            if (buf != null)
            {
                buf.Parent = root;
            }
        }
        public static void RotateRL(ref BinaryTree root)
        {
            BinaryTree bufLeft = root.Right.Left.Left;
            BinaryTree bufRight = root.Right.Left.Right;
            if (root.Parent == null)
            {
                root.Right.Left.Parent = null;
            }
            else
            {
                root.Right.Left.Parent = root.Parent;

                if (root.Parent.Left == root)
                {
                    root.Parent.Left = root.Parent.Left = root.Right.Left;
                }
                else
                {
                    root.Parent.Right = root.Right.Left;
                }
            }

            root.Right.Left.Left = root;
            root.Right.Left.Right = root.Right;
            root.Right.Parent = root.Right.Left;
            root.Parent = root.Right.Left;
            root.Right.Left = bufRight;
            root.Right = bufLeft;
            if (bufRight != null)
            {
                bufRight.Parent = root.Right;
            }
            if (bufLeft != null)
            {
                bufLeft.Parent = root;
            }
        }
        public static void RotateLR(ref BinaryTree root)
        {
            BinaryTree bufLeft = root.Left.Right.Left;
            BinaryTree bufRight = root.Left.Right.Right;
            if (root.Parent == null)
            {
                root.Left.Right.Parent = null;
            }
            else
            {
                root.Left.Right.Parent = root.Parent;
                if (root.Parent.Left == root)
                {
                    root.Parent.Left = root.Left.Right;
                }
                else
                {
                    root.Parent.Right = root.Left.Right;
                }
            }
            root.Left.Right.Right = root;
            root.Left.Right.Left = root.Left;
            root.Left.Parent = root.Left.Right;
            root.Parent = root.Left.Right;
            root.Left.Right = bufLeft;
            root.Left = bufRight;
            if (bufLeft != null)
            {
                bufLeft.Parent = root.Left;
            }
            if (bufRight != null)
            {
                bufRight.Parent = root;
            }
        }
        public static void RotateLL(ref BinaryTree root)
        {
            BinaryTree buf = root.Left.Right;
            if (root.Parent == null)
            {
                root.Left.Parent = null;
            }
            else
            {
                root.Left.Parent = root.Parent;
                if (root.Parent.Left == root)
                {
                    root.Parent.Left = root.Left;
                }
                else
                {
                    root.Parent.Right = root.Left;
                }
            }
            root.Parent = root.Left;
            root.Left.Right = root;
            root.Left = buf;
            if (buf != null)
            {
                buf.Parent = root;
            }
        }
        public static BinaryTree FindNS(BinaryTree root)
        {
            if (root == null)
                return root;
            var queue = new Queue<BinaryTree>();
            queue.Enqueue(root);
            while (true)
            {
                if (queue.Peek().Left != null)
                {
                    queue.Enqueue(queue.Peek().Left);
                }
                if (queue.Peek().Right != null)
                {
                    queue.Enqueue(queue.Peek().Right);
                }
                if ((queue.Peek().Left == null && queue.Peek().Right == null)) return queue.Peek();
                queue.Dequeue();
            }
        }
        public static long? DataBalance(BinaryTree root)
        {
            if (root == null)
            {
                return null;
            }
            long? difference = HeightTreeFind(FindNS(root.Right), root) - HeightTreeFind(FindNS(root.Left), root);
            return difference;
        }
        public static bool BalanceCheck(BinaryTree root)
        {
            long? difference = DataBalance(root);
            if (difference == null) return true;
            if (difference <= 1 && difference >= -1)
            {
                return true;
            }
            return false;
        }
        public static void BalanceTree(BinaryTree root)
        {
            long?[] array;
            while (true)
            {
                root = TreeFind(root);
                array = ToArray(root);
                Array.Reverse(array);
                var bufTree = new BinaryTree();
                var bufSheet = new BinaryTree();
                bool flag = true;
                foreach (var i in array)
                {
                    bufTree = root.Find(i);
                    if (BalanceCheck(bufTree))
                    {
                        continue;
                    }
                    else
                    {
                        flag = false;
                        var leftSheet = FindNS(bufTree.Left);
                        var rightSheet = FindNS(bufTree.Right);
                        bufSheet = HeightTreeFind(rightSheet, bufTree) > HeightTreeFind(leftSheet, bufTree) ? rightSheet : leftSheet;
                        if (bufSheet == rightSheet)
                        {
                            if (rightSheet == rightSheet.Parent.Left)
                            {
                                RotateRL(ref bufTree);
                            }
                            else
                            {
                                RotateRR(ref bufTree);
                            }
                        }
                        else
                        {
                            if (leftSheet == leftSheet.Parent.Right)
                            {
                                RotateLR(ref bufTree);
                            }
                            else
                            {
                                RotateLL(ref bufTree);
                            }
                        }
                        break;
                    }
                }
                if (flag)
                {
                    break;
                }
            }
        }
    }
}