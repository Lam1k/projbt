﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjBinaryTree
{
    public enum BinSide
    {
        Left,
        Right
    }
    public class BinaryTree : Parametres
    {
        /// <summary>
        /// Вставляет целочисленное значение в дерево
        /// </summary>
        /// <param name="data">Значение, которое добавится в дерево</param>
        public void Insert(long data)
        {
            if (Data == null || Data == data)
            {
                Data = data;
                return;
            }
            if (Data > data)
            {
                if (Left == null) Left = new BinaryTree();
                Insert(data, Left, this);
            }
            else
            {
                if (Right == null) Right = new BinaryTree();
                Insert(data, Right, this);
            }
        }
        /// <summary>
        /// Вставляет значение в определённый узел дерева
        /// </summary>
        /// <param name="data">Значение</param>
        /// <param name="node">Целевой узел для вставки</param>
        /// <param name="parent">Родительский узел</param>
        private void Insert(long data, BinaryTree node, BinaryTree parent)
        {

            if (node.Data == null || node.Data == data)
            {
                node.Data = data;
                node.Parent = parent;
                return;
            }
            if (node.Data > data)
            {
                if (node.Left == null) node.Left = new BinaryTree();
                Insert(data, node.Left, node);
            }
            else
            {
                if (node.Right == null) node.Right = new BinaryTree();
                Insert(data, node.Right, node);
            }
        }
        /// <summary>
        /// Уставляет узел в определённый узел дерева
        /// </summary>
        /// <param name="data">Вставляемый узел</param>
        /// <param name="node">Целевой узел</param>
        /// <param name="parent">Родительский узел</param>
        private void Insert(BinaryTree data, BinaryTree node, BinaryTree parent)
        {

            if (node.Data == null || node.Data == data.Data)
            {
                node.Data = data.Data;
                node.Left = data.Left;
                node.Right = data.Right;
                node.Parent = parent;
                return;
            }
            if (node.Data > data.Data)
            {
                if (node.Left == null) node.Left = new BinaryTree();
                Insert(data, node.Left, node);
            }
            else
            {
                if (node.Right == null) node.Right = new BinaryTree();
                Insert(data, node.Right, node);
            }
        }
        /// <summary>
        ///Определяет, в какой ветви для родительского лежит данный узел
        /// </summary>
        /// <param name = "node" ></ param >
        /// < returns ></ returns >
        private BinSide? SideForParent(BinaryTree node)
        {
            if (node.Parent == null) return null;
            if (node.Parent.Left == node) return BinSide.Left;
            if (node.Parent.Right == node) return BinSide.Right;
            return null;
        }

        /// <summary>
        /// Ищет узел с заданным значением
        /// </summary>
        /// <param name = "data" > Значение для поиска</param>
        /// <returns></returns>
        public BinaryTree Find(long? data)
        {
            if (Data == data) return this;
            if (Data > data)
            {
                return Find(data, Left);
            }
            return Find(data, Right);
        }
        /// <summary>
        /// Ищет значение в определённом узле
        /// </summary>
        /// <param name = "data" > Значение для поиска</param>
        /// <param name = "node" > Узел для поиска</param>
        /// /// <returns></returns>
        public BinaryTree Find(long? data, BinaryTree node)
        {
            if (node == null) return null;

            if (node.Data == data) return node;
            if (node.Data > data)
            {
                return Find(data, node.Left);
            }
            return Find(data, node.Right);
        }

        /// <summary>
        /// Удаляет узел из дерева
        /// </summary>
        /// <param name="node">Удаляемый узел</param>
        public void Remove(BinaryTree node)
        {
            if (node == null) return;
            var me = SideForParent(node);
            //Если у узла нет дочерних элементов, его можно смело удалять
            if (node.Left == null && node.Right == null)
            {
                if (me == BinSide.Left)
                {
                    node.Parent.Left = null;
                }
                else
                {
                    node.Parent.Right = null;
                }
                return;
            }
            //Если нет левого дочернего, то правый дочерний становится на место удаляемого
            if (node.Left == null)
            {
                if (me == BinSide.Left)
                {
                    node.Parent.Left = node.Right;
                }
                else
                {
                    node.Parent.Right = node.Right;
                }

                node.Right.Parent = node.Parent;
                return;
            }
            //Если нет правого дочернего, то левый дочерний становится на место удаляемого
            if (node.Right == null)
            {
                if (me == BinSide.Left)
                {
                    node.Parent.Left = node.Left;
                }
                else
                {
                    node.Parent.Right = node.Left;
                }

                node.Left.Parent = node.Parent;
                return;
            }
            //Если присутствуют оба дочерних узла
            //то правый ставим на место удаляемого
            //а левый вставляем в правый
            if (me == BinSide.Left)
            {
                node.Parent.Left = node.Right;
            }
            if (me == BinSide.Right)
            {
                node.Parent.Right = node.Right;
            }
            if (me == null)
            {
                var bufLeft = node.Left;
                var bufRightLeft = node.Right.Left;
                var bufRightRight = node.Right.Right;
                node.Data = node.Right.Data;
                node.Right = bufRightRight;
                node.Right = bufRightRight;
                node.Right = bufRightRight;
                node.Left = bufRightLeft;
                Insert(bufLeft, node, node);
            }
            else
            {
                node.Right.Parent = node.Parent;
                Insert(node.Left, node.Right, node.Right);
            }
        }
        /// <summary>
        /// Удаляет значение из дерева
        /// </summary>
        /// <param name="data">Удаляемое значение</param>
        public void Remove(long data)
        {
            var removeNode = Find(data);
            if (removeNode != null)
            {
                Remove(removeNode);
            }
        }
        public static BinaryTree TreeFind(BinaryTree node)
        {
            if (node.Parent == null)
            {
                return node;
            }
            return TreeFind(node.Parent);
        }
        public static long HeightTreeFind(BinaryTree node, BinaryTree parent, long height = 0)
        {
            if (node == null)
            {
                return 0;
            }
            height++;
            if (node.Parent == parent)
            {
                return height;
            }
            return HeightTreeFind(node.Parent, parent, height);
        }
        /// <summary>
        /// Количество элементов в определённом узле
        /// </summary>
        /// <param name="node">Узел для подсчета</param>
        /// <returns></returns>
        private long CountElements()
        {
            long count = 0;
            if (Right != null)
            {
                ++count;
                count += Right.CountElements();
            }
            if (Left != null)
            {
                ++count;
                count += Left.CountElements();
            }
            return count;
        }

        public static void RemoveSubtree(BinaryTree tree, long? rootData)
        {
            tree = tree.Find(rootData);
            if (tree == null)
            {
                return;
            }
            if (tree.Parent == null && tree.Left == null && tree.Right == null)
            {
                tree = null;
            }
            {
                BinaryTree bufParent = tree;
                long?[] array = ToArray(tree);
                Array.Reverse(array);
                foreach (int i in array)
                {
                    tree.Remove(tree.Find(i));
                }
                tree = BinaryTree.TreeFind(bufParent);
                tree = null;
            }
        }
        public static long?[] ToArray(BinaryTree subtree)
        {
            long element = subtree.CountElements() + 1;
            long?[] array = new long?[element];
            long i = 0;
            ToArray(subtree, array, ref i);
            return array;
        }
        private static long?[] ToArray(BinaryTree subtree, long?[] array, ref long i)
        {
            if (subtree != null)
            {
                if (subtree.Parent == null)
                {
                    array[i++] = subtree.Data;
                }
                else
                {
                    if (subtree.Parent.Left == subtree)
                    {
                        array[i++] = subtree.Data;
                    }
                    if (subtree.Parent.Right == subtree)
                    {
                        array[i++] = subtree.Data;
                    }
                }
                if (subtree.Left != null)
                {
                    ToArray(subtree.Left, array, ref i);
                }
                if (subtree.Right != null)
                {
                    ToArray(subtree.Right, array, ref i);
                }
            }
            return array;
        }
    }
}
