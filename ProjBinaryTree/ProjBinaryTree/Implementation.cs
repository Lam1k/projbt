﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjBinaryTree
{
    public class Implementation
    {
        public static void Run()
        {
            var tree = new BinaryTree();

            tree.Insert(10);
            tree.Insert(20);
            tree.Insert(30);
            tree.Insert(40);
            tree.Insert(50);
            tree.Insert(69);
            tree.Insert(71);
            tree.Insert(82);
            tree.Remove(71);
            tree.Insert(90);
            tree.Remove(50);
            tree.Insert(104);

            BinaryTreeExtensions.Print(tree);
            TreeBalancing.BalanceTree(tree);
            tree = BinaryTree.TreeFind(tree);
            BinaryTreeExtensions.Print(tree);

            Console.ReadKey();

        }

    }
}
